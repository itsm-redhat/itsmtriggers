# Driving ITSM Workload through Ansible Tower

The project here describes and demonstrates the how ServiceNow change requests can be mapped to drive ansible tower jobs to resolve issues.

This project / article aims to fit into an existing operating change-management culture by an iterative approach ( 1 automation at a time ) i.e. doesn try to offload everything into automation as big bang.

These play books provide a variety of mechanisms and can be retro fitted to work with most ITSM products who support REST API's

## The Article  environment

The Article below describes / show cases how ansible tower can drive / automate service now tasks

These play books provide a variety of mechanisms and can be retro fitted to work with most ITSM products who support REST API's not just Service Now

## Git repos
- This git repo container the plabooks necessary to trigger jobs

To project aims to fit inside an existing operating change-management culture as such lets define a scenario

#### Scenario

Lets create a master request  ( this can be a change_request, or a problem, or a catalog record)
This master request has say 3 subtasks ( this can another type of record like change_tasks, relese_records, incidents_tasks etc) , of the 3 subtasks
- task1: automated
- task2: automated
- task3: manual

In this flow we assume that tasks are atomic and no interdependencies, it is also possible to sequence with a few minor tweaks. However I would recommed tasks be atomic and not dependant as dependencies often need manual inputs to be put into a job i.e. from an earlier task to an automated tasks.

#### The Trigger Flow

In the article below our setup is as follows

```mermaid
graph TD;
  CR(Create Change Request)-->CAssignee;
  CAssignee(Map an Assignee and owner)-->CT;
  CT(Create Change Tasks) --> Assign
  Assign(Set Custom change task Field u_automation to Auto or manual)-->Approvals;
  Approvals(Request Change Approvals flow) --> CRTrigger
  CRTrigger( Change reaches the state 'Scheduled');


  TowerPolling(Tower polls Service now for CRs in state 'Scheduled')-->CRTrigger
  TowerPolling-->OnCRSFound
  OnCRSFound(On CRs matching search criteria) --> GetTasks
  GetTasks( Get Tasks mapped for Automation via the u_automation field) --> setTasksReady
  setTasksReady --> TriggerTower
  TriggerTower( Trigger tower jobs for each automatable task as per rules.yaml )

```
#### The Closure Flow
-  Git Repo [Role](https://gitlab.com/itsm-redhat/dummy-ansible-play) provides a default role which can be used to close change management tickets

The Trigger flows sends 2 extra vars into ansible jobs it triggers this can be used to close the Change Task
The above gitrepo provides a default rols whch takes this input and closes the Change Task

Similar setup can be done to close master record if necessary

## Configuring the Instance

Edit these files

- setting_core.yaml  --> for different kinds of search and commits
- settings_adv.yaml  --> for URL construction
- rule.yaml          -->  trigger different jobs based on conditions
